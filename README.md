# Oracle Weblogic Statistics exporter to InfluxDB.

Weblogic_stats uses service from web application like WebLogic Monitoring Exporter.
It gets Prometheus-compatible metrics from URL, and exports into InfluxDB databases.

Installation:

    Get source from this gitlab repository
    make
    Copy weblogic_stats executable
    Modify config.yml as needed (Weblogic URLs, authentication informations, InfluxDB connection)

Option:

    weblogic_stats --config autre_config.yml


Configuration:

    Create an YAML file like this:
    
```yaml
influxdb:
    server: @IP du serveur InfluxDB
    port: Port du serveur (usually 8086)
    user: database username or empty
    pwd: user password or empty
    base: database name

mesures:
- name: Statistics group

  auth: 'username:password' omits if not used.

  urls: ['http://machine:port/path',
        'http://next URL']

  filtre: ['chaine1', 'chaine2',
           '...', ....
           'statN-1', 'Stat N' ]
           
  frequency: 40

- name: another_group

  auth: '....:....'

  urls: ['http://...']
         
  filtre: ['.....']
  
```
- In 'mesures' stanza, be carefull with blanks and minus, it's YAML syntax ! :-)
- If omitted frequency = 10 seconds.
- name is a tag for all following urls into InfluxDB database.
  