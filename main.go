package main

import (
	"os"
	"fmt"
	"flag"
	"time"
	"sync"
	"bufio"
	"strings"
	"net/url"
	"net/http"
	"html/template"
	"net/http/pprof"
	
	"github.com/apex/log"
	"github.com/apex/log/handlers/cli"
	
	"gitlab.com/ecoc/weblogic_stats/config"
	"gitlab.com/ecoc/weblogic_stats/influx"
)

// External refs
var Version string
var Build	string

var debug = flag.Bool( "debug", false, "Show debug log" )
var configFile = flag.String( "config", "config.yml", "config file" )
var listenPort = flag.String( "port", ":9444", "listen port" )
var version = flag.Bool( "version", false, "Prints current version." )

var indexTmpl = `
<html>
<head><title>WebLogic stat exporter to InfluxDB agent</title></head>
<body>
	<h1>WebLogic to InfluDB</h1>
	<h3>Configuration</h3>
	<ul>
	 {{if .InfluxCnx.Server}} <li>Serveur: {{.InfluxCnx.Server}}:{{.InfluxCnx.Port}} </li> {{end}}
	 {{if .InfluxCnx.User}} <li>User: {{.InfluxCnx.User}} </li> {{end}}
	 {{if .InfluxCnx.Base}} <li>Base: {{.InfluxCnx.Base}} </li> {{end}}
	 {{if .InfluxCnx.Retention}} <li>Retention: {{.InfluxCnx.Retention}} </li> {{end}}
	</ul>
	<h3>WebLogic URls being monitored:</h3>
	<ul>
	{{ range .Mesures }}
		<li>{{.Name}}</li>
		<ul>
			{{ range $index, $element := .Urls }}
				<li><a href="{{ $element }}">{{$element}}</a></li>
			{{ end }}
		</ul>
	{{ end }}
	</ul>
</body>
</html>
`
var statsTmpl = `
<html>
<head><title>WebLogic stat exporter to InfluxDB agent</title></head>
<body>
	<h1>WebLogic to InfluxDB</h1>
	<h3>Statistics</h3>
	<ul>
		<li>Début: {{.Start}}</li>
		<li>Nombre de lignes lues: {{.Lines}}</li>
		<li>Nombre de mesures conservées: {{.Mesures}}</li>
	</ul>
</body>
</html>
`
var unflux *influx.BaseInfos

// Statistiques
type WebLoStats_stats struct {
	Start		string
	Lines		uint64	
	Mesures		uint64
}

var mutex	sync.Mutex
var stats	WebLoStats_stats

func readWebLogicStats( mesure config.WebLogic, instance int ) {
	instance_str := fmt.Sprintf( "%d", instance )
	var ctxLog = log.WithFields(log.Fields{
					"Appli":	"weblogic_stats",
					"Name":		mesure.Name,
					"instance":	instance_str,
		})
	ctxLog.Info( "Reading" )
	
	URL, err := url.Parse( mesure.Urls[instance] )
	hostname := strings.Split(URL.Hostname(),".")[0]
	port := URL.Port()
	ctxLog.Debugf( "Hostname: %s, Port: %s", hostname, port )
	
	req, err := http.NewRequest( "GET", mesure.Urls[instance], nil )
	if err != nil {
		ctxLog.WithError( err ).Fatal( "Newrequest" )
	}
	if mesure.Auth != "" {
		auth := strings.Split( mesure.Auth, ":" )
		req.SetBasicAuth( auth[0], auth[1] )
	}
	storage, err := influx.NewStorageInfos( unflux )	
	if err != nil {
		ctxLog.WithError( err ).Fatal( "NewStorageInfos" )
	}
ReadingLoop:
	for {
		ctxLog.Debugf( "Calling URL: %s", mesure.Urls[instance] )
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			ctxLog.WithError(err).Error("failed to read url")
			time.Sleep( 60 )
			continue ReadingLoop
		}
		defer resp.Body.Close()
		
		scanner := bufio.NewScanner( resp.Body )		// breaks response into lines
		for scanner.Scan() {
			line := scanner.Text()
//			ctxLog.Debugf("new line: %s", line )
			mutex.Lock()
			stats.Lines++
			mutex.Unlock()
			if mesure.Filtres != nil {
				for _, filtre := range mesure.Filtres {
					if strings.Contains( line, filtre ) {	// WARNING. Don't care about similar substrings !!!
						influx.AddMeasures( unflux, storage, hostname, port, mesure.Name, line, mesure.Tags )
						mutex.Lock()
						stats.Mesures++
						mutex.Unlock()
						break
					}
				}
//			} else {
//				influx.AddMeasures( unflux, storage, hostname, port, mesure.Name, line, mesure.Tags )
//				mutex.Lock()
//				stats.Mesures++
//				mutex.Unlock()
			}
			continue
		}
		resp=nil
		time.Sleep( time.Second * mesure.Freq )
	}
	ctxLog.Warn( "Url stop reporting" )
}

func init() {
	log.SetHandler( cli.Default )
	stats.Start = time.Now().String()
	stats.Lines = 0
	stats.Mesures = 0
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf( os.Stderr, "usage: weblogic_stats [option]\n" )
		flag.PrintDefaults()
		os.Exit( 2 )
	}
	flag.Parse()

	if *version {
		fmt.Fprintf( os.Stdout, "Version: %s\n", Version )
		fmt.Fprintf( os.Stdout, "Build Time: %s\n", Build )
		os.Exit( 0 )
	}
	
	if *debug {
		log.SetLevel( log.DebugLevel )
	} else {
		log.SetLevel( log.ErrorLevel )
	}
	
	ctxLog := log.WithFields( log.Fields{
						"appli":	"weblogic_stats",
						"config:":	*configFile,
	})
	
	conf, err := config.Parse( *configFile )
	if err != nil {
		ctxLog.WithError(err).Fatalf( "failed to parse config file: %s", *configFile )
	}
	ctxLog.Infof( "Configuration: %#v", conf )
	//
	// Open connection to InfluxDB.
	//
	unflux = influx.ConnectToInflux( conf )
	ctxLog.Infof( "Connected to InfluxDB: %#v", unflux )
	//
	// Start WebLogic stats readers
	//
	for rang, mesure := range conf.Mesures {
		if mesure.Freq < 10 {
			mesure.Freq = 10
		}
		if mesure.Name == "" {
			ctxLog.Fatalf( "mesure #%d: group name mandatory", rang )
		}
		for indice, _ := range mesure.Urls {
			ctxLog.Infof( "Processing: %d of %s", indice, mesure.Name )
			go readWebLogicStats( mesure, indice )
		}
	}
	//
	// start web server
	//
	var mux = http.NewServeMux()
	var index = template.Must(template.New("index").Parse(indexTmpl))
	var stats_pg = template.Must(template.New("stats").Parse(statsTmpl))
	
	mux.HandleFunc( "/",
		func( w http.ResponseWriter, r *http.Request ) {
			if err := index.Execute(w, &conf); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
		})
	mux.HandleFunc( "/stats",
		func( w http.ResponseWriter, r *http.Request ) {
			if err := stats_pg.Execute(w, &stats); err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
			}
		})

	mux.HandleFunc( "/debug/pprof/", pprof.Index )
	mux.HandleFunc( "/debug/pprof/cmdline", pprof.Cmdline )
	mux.HandleFunc( "/debug/pprof/profile", pprof.Profile )
	mux.HandleFunc( "/debug/pprof/symbol", pprof.Symbol )

	mux.Handle( "/debug/pprof/goroutine", pprof.Handler("goroutine") )
	mux.Handle( "/debug/pprof/heap", pprof.Handler("heap") )	
	mux.Handle( "/debug/pprof/threadcreate", pprof.Handler("threadcreate") )
	mux.Handle( "/debug/pprof/block", pprof.Handler("block") )

	ctxLog.Info( "Starting http server" );
	if err := http.ListenAndServe(*listenPort, mux); err != nil {
		ctxLog.WithError(err).Fatal( "failed to start server" )
	}
}