package influx

import (
	"time"
	"regexp"
	"strings"
	"strconv"
	"runtime/debug"
	"github.com/apex/log"

	client "github.com/influxdata/influxdb1-client/v2"

	"gitlab.com/ecoc/weblogic_stats/config"
)

const (
	TEMPO		 = 600		// Nombre de secondes maximum entres deux envois.
	MAX_MEASURE	 = 140		// Nombre de mesures maximum stockées avant envois (~400sec).
)

type BaseInfos struct {
	// client	InfluxDB connection handler
	client	client.Client
	// batchPointsConf configuration data needed to create 
	// a BatchPoints instance. At least Database name and Precision.
	batchPointsConf	client.BatchPointsConfig
}

type LastValue struct {
	// nextSendMap time of of next write
	nextSendMap			time.Time
	// lastIsOpen	value of last isOpen data
	lastIsOpen			bool
}

type StorageInfos struct {
	// batchPointsTable give access to BatchPoints struct for each cluster
	batchPointsTable	client.BatchPoints
	// batchPointsNumber store BatchPoints number for each cluster
	batchPointsNumber	int
	// lastValues store time of next write and last value of isOpen
	lastValues			map[string]*LastValue
}

// getNewBatch creates a new batch points table for influxDB.
func (tf *BaseInfos)getNewBatch() ( client.BatchPoints, error ) {
	debug.FreeOSMemory()		// Force GC and attempts to return as much memory to the OS.
	return client.NewBatchPoints( tf.batchPointsConf )
}

// QueryDB will send query to InfluxDB database.
func (tf *BaseInfos)QueryDB( q client.Query ) (*client.Response, error) {
	var ctxLog = log.WithFields( log.Fields{
					"Appli":	"weblogic_stats",
					"Base":		tf.batchPointsConf.Database,
	})

	response, err := tf.client.Query( q )

	if err != nil {
		ctxLog.WithError(err).Error( "QueryDB error" )
	} else if response.Error() != nil {
		ctxLog.WithError(response.Error()).Error( "QueryDB response error" )
	} else {
		ctxLog.Debugf( "QueryDB response: %v.\n", response.Results )
	}

	if err != nil {
		return nil, err
	} else if response.Error() != nil {
		return nil, response.Error()
	} else {
		return response, nil
	}
}

// CreateDB is used to create a new database.
// If database already exists, no error will return.
func (tf *BaseInfos)CreateDB() error {
	var ctxLog = log.WithFields( log.Fields{
					"Appli":	"weblogic_stats",
					"Base":		tf.batchPointsConf.Database,
	})

	q := client.NewQuery( "CREATE DATABASE " + tf.batchPointsConf.Database, "", "" )
	response, err := tf.client.Query( q )
	if err != nil {
		return err
	}
	ctxLog.Debugf( "CREATE DATABASE %s: %s.\n", tf.batchPointsConf.Database, response.Results )

	return nil
}

// CreateRetentionPolicy creates an InfluDB database retention policy 
// These becomes new Stockage default retention.
func (tf *BaseInfos)CreateRetentionPolicy( ret_name string, duration string, defaut bool ) error {
	var ctxLog = log.WithFields( log.Fields{
					"Appli":	"weblogic_stats",
					"Base":		tf.batchPointsConf.Database,
	})

	ctxLog.Debugf( "SetRetentionPolicy(%s,%s,%v)\n", ret_name, duration, defaut )

	cmd := "DURATION " + duration + "REPLICATION 1"
	if defaut {
		cmd += " DEFAULT"
	}

	query := "CREATE RETENTION POLICY \"" + ret_name + "\" ON \"" + tf.batchPointsConf.Database + "\" " + cmd
	q := client.NewQuery( query, "", "" )
	response, err := tf.client.Query( q )
	if err != nil || response.Error() != nil {
		if err != nil {
			return err
		} else {
			return response.Error()
		}
	}

	tf.batchPointsConf.RetentionPolicy = ret_name

	return nil
}

// Flush send batch points to InfluxDB.
func (tf *BaseInfos)Flush( st *StorageInfos ) error {
	err := tf.client.Write( st.batchPointsTable )
	if err != nil {
		return err
	}
	bp, err := tf.getNewBatch()
	if err != nil {
		return err
	}
	st.batchPointsNumber = 0;
	st.batchPointsTable = nil;
	st.batchPointsTable = bp

	return nil
}
// NewStorageInfos creates a new batchpoints for each cluster because
// threading requirements.
//
func NewStorageInfos( base *BaseInfos ) (*StorageInfos, error) {
	var ctxLog = log.WithFields( log.Fields{
					"Appli":	"weblogic_stats",
					"Base":		base.batchPointsConf.Database,
	})
	var err error

	tf := new(StorageInfos)
	tf.batchPointsTable, err = client.NewBatchPoints( base.batchPointsConf )
	if err != nil {
		ctxLog.WithError(err).Error( "NewBatchPoints" )
		return nil,err
	}
	tf.batchPointsNumber = 0

	return tf,nil
}

// ConnectToInflux creates connexion context with InfluxDB server.
func ConnectToInflux( conf config.Config ) *BaseInfos {
	var ctxLog = log.WithFields( log.Fields{
					"Appli":	"weblogic_stats",
					"Server":	conf.InfluxCnx.Server,
					"Base":		conf.InfluxCnx.Base,
	})
	var err error

	stock := new(BaseInfos)
	addresse := "http://" + conf.InfluxCnx.Server + ":" + conf.InfluxCnx.Port
	if conf.InfluxCnx.User != "" {
		stock.client, err = client.NewHTTPClient(client.HTTPConfig{
					Addr: addresse,
					Username: conf.InfluxCnx.User,
					Password: conf.InfluxCnx.Password,
		})
	} else {
		stock.client, err = client.NewHTTPClient(client.HTTPConfig{
					Addr: addresse,
		})
	}
	if err != nil {
		ctxLog.WithError(err).Fatal("failed to connect InfluxDB")
	}

	defer stock.client.Close()

	stock.batchPointsConf = client.BatchPointsConfig{
					Database: conf.InfluxCnx.Base,
					Precision: "ns",
	}

	err = stock.CreateDB()
	if err != nil {
		ctxLog.WithError(err).Fatal("failed to create database")
	}

	if conf.InfluxCnx.Retention != "" {
		stock.batchPointsConf.RetentionPolicy = conf.InfluxCnx.Retention
	}

	return stock
}

// AddMeasures add measurement into batchPointsTable and send datas on regular interval.
//
// Format de line: measurement{tag_name=tag_value[,tag_name=tag_value,...]} field_value
//
func AddMeasures( tf *BaseInfos, st *StorageInfos, host string, port string, group string, line string, tags_env []string ) error {
	var ctxLog = log.WithFields( log.Fields{
					"Appli":	"weblogic_stats",
					"Host":		host,
					"Base":		tf.batchPointsConf.Database,
	})

	stamp := time.Now()

	ctxLog.Infof( "AddMeasures: %v now: %v", line, stamp )
	
	re_measure := regexp.MustCompile( "(.*){" )
	re_tag := regexp.MustCompile( `([^,{}=]+)=([^,{}=]+)` )
	re_value := regexp.MustCompile( `}[ ]+(.*)` )
	re_subtag := regexp.MustCompile( `([^!+)!([^@\.]+)[@\.](.+)$` )
	
	measurement := re_measure.FindAllStringSubmatch(line,-1)[0][1]
	
	tags := map[string]string {
		"host": host,
		"port": port,
		"group":group,
	}
	
	if tags_env != nil {
		for _, tag := range tags_env {
			vals := strings.Split( tag, "=" )
			tags[vals[0]] = strings.Trim(vals[1],`"`)
		}
	}
	
	tags_values := re_tag.FindAllStringSubmatch(line,-1)
	for _, val := range tags_values {
		tags[val[1]] = strings.Trim(val[2],`"`)
		if val[1] == "destination" {
			// explode queue name into subtags
			sub_tags := re_subtag.FindAllStringSubmatch(tags[val[1]],-1)[0][0:]
			for idx, sub_tag_val := range sub_tags {
				tags["dest_part"+strconv.Itoa(idx)] = sub_tag_val
			}
		}
	}
	
	s, err := strconv.ParseFloat(re_value.FindAllStringSubmatch(line,-1)[0][1], 64)
	if err != nil {
		ctxLog.WithError(err).Error( "Conversion de résulat impossible." )
		return err
	}
	fields := map[string]interface{}{"value":s}
	
	pt, err := client.NewPoint( measurement, tags, fields, stamp )
	if err != nil {
		ctxLog.WithError(err).Warn("failed to create point")
	} else {
		st.batchPointsTable.AddPoint(pt)
		st.batchPointsNumber++;
	}
	// Send infos to InfluxDB
	err = nil
	if st.batchPointsNumber >= MAX_MEASURE {
		ctxLog.Debugf( "Sending %d measurements to InfluxDB at %s", st.batchPointsNumber, stamp.String() )

		err := tf.Flush( st )
		if err != nil {
			ctxLog.WithError(err).Error( "Flushing measurements" )
		}
	}

	return err
}
