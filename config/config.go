package config

import (
	"time"
	"io/ioutil"
	
	"gopkg.in/yaml.v2"
)

// InfluxDB server connect

type Storage struct {
	Server		string	`yaml:"server,omitempty"`
	Port		string	`yaml:"port,omitempty"`
	User		string	`yaml:"user,omitempty"`
	Password	string	`yaml:"pwd,omitempty"`
	Base		string	`yaml:"base,omitempty"`
	Retention	string	`yaml:"retention,omitempty"`
}

// URL to stat webservice.

type WebLogic struct {
	Name	string			`yaml:"group,omitempty"`
	Urls	[]string		`yaml:"urls,omitempty"`
	Auth	string			`yaml:"auth,omitempty"`
	Tags	[]string		`yaml:"tags,omitempty"`
	Filtres	[]string		`yaml:"filtre,omitempty"`
	Freq	time.Duration	`yaml:"frequency,omitempty"`
}

type Config struct {
	InfluxCnx	Storage		`yaml:"influxdb,omitempty"`
	Mesures		[]WebLogic	`yaml:"mesures,omitempty"`
}
// Parse a file into a config instance
func Parse(f string) (config Config, err error) {
	bts, err := ioutil.ReadFile(f)
	if err != nil {
		return
	}
	err = yaml.Unmarshal(bts, &config)

	return
}

